<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<title>Tambah Data</title>

<style>
* {
  box-sizing: border-box;
}

input[type=text],input[type=number], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body>
<h1> </h1>
<div class="container">
<h2>Tambah Data</h2>
  <form method="post" action="insert" class="card-body">
    <div class="row">
      <div class="col-25">
        <label for="fname">Username</label>
      </div>
      <div class="col-75">
        <input type="text" name="username" placeholder="username">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Full name</label>
      </div>
      <div class="col-75">
        <input type="text" name="fullname" placeholder="full-name">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Address</label>
      </div>
      <div class="col-75">
        <input type="text" name="address" placeholder="Address">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Status</label>
      </div>
      <div class="col-75">
        <input type="text" name="status" placeholder="Status">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Grades Physics</label>
      </div>
      <div class="col-75">
        <input type="number" name="physics" placeholder="grades Physics">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Grades Calculus</label>
      </div>
      <div class="col-75">
        <input type="number" name="calculus" placeholder="grades Calculus">
      </div>
    </div>

    <div class="row">
      <div class="col-25">
        <label for="lname">Grades Biologi</label>
      </div>
      <div class="col-75">
        <input type="number" name="biologi" placeholder="grades Calculus">
      </div>
    </div>

    <div class="row">
      <input type="submit" value="Submit">
    </div>
  </form>
</div>

</body>
</html>
